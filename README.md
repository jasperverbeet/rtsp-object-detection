# Reducing false positives in security cameras

```
docker run -it -v '{absolute_path_to_project}/project:/code' jasperverbeet/rtsp-opencv:latest python3 main.py rtsp://184.72.239.149/vod/mp4:BigBuckBunny_175k.mov 1
```

The first paramater is the rtsp stream, the second one the amount of threads. Make sure you fill in the `{absolute_path_to_project}`.

![Photo](images/1.png)