import sys
import numpy as np
import cv2
import time
from threading import Thread
from processor import FramePredictor

if len(sys.argv) == 1:
    print('Please supply an rtsp stream')
    exit(1)

INPUT_VID = sys.argv[1]
THREADS = 1
if len(sys.argv) == 3:
    THREADS = sys.argv[2]

print('Opening stream: {} ...'.format(INPUT_VID))

# cap.set(cv2.CAP_PROP_BUFFERSIZE,1)
vcap = cv2.VideoCapture(INPUT_VID)

class Queue:
    def __init__(self, max_limit=1):
        self.q = []
        self.l = max_limit

    def _lowest(self):
        # Get lowest value
        queue_keys = list(map(lambda x: x.get('frame_count'), self.q))
        val = min(queue_keys)
        return queue_keys.index(val)

    def put(self, item):
        if len(self.q) >= self.l:
            self.q[self._lowest()] = item
            return None

        self.q.append(item)

    def get(self):
        index = self._lowest()
        item = self.q[index]
        del self.q[index]
        return item


    def empty(self):
        return len(self.q) == 0

class VideoProcessor:
    def __init__(self, *args, **kwargs):
        num_worker_threads = int(THREADS)
        self.queue = Queue(max_limit=num_worker_threads)
        print('Starting up {} threads ...'.format(num_worker_threads))
        for i in range(num_worker_threads):
            t = Thread(target=self.worker, args=(i,))
            t.start()
            # Slight delay between threads
            time.sleep(.2)

    def worker(self, *args, **kwargs):
        predictor = FramePredictor()

        while True:
            if self.queue.empty():
                continue

            frame_data = self.queue.get()
            predictor.predict(
                frame_data.get('frame_content'),
                frame_data.get('frame_count'),
                thread=args[0]+1,
            )

processor = VideoProcessor()
frame_count = 0

while True:
    ret, frame = vcap.read()

    if not ret:
        print('Skipping empty frame...')
        continue

    try:
        processor.queue.put(dict(
            frame_content=frame,
            frame_count=frame_count,
        ))
        frame_count += 1
        # print(frame_count)
    except:
        pass

vcap.release()