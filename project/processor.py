import numpy as np
import cv2
import time


class FramePredictor:
    
    def __init__(self, *args, **kwargs):
        print('Initializing scanner ...')

        self.YOLO_CLASSES = '/code/yolov3.txt'
        self.YOLO_WEIGHTS = '/weights/yolov3.weights'
        self.YOLO_DNN     = '/code/yolov3.cfg'
        self.CONF_THRES   = 0.5
        self.NMS_THRES    = 0.4
        self.SCALE        = 0.00392
        self.REC_TIME     = 70
        self.FRAME_RATE   = 15
        self.CLASSES      = None
        self.COLORS       = None
        
        with open(self.YOLO_CLASSES, 'r') as f:
            self.CLASSES = [line.strip() for line in f.readlines()]
            self.COLORS = np.random.uniform(0, 255, size=(len(self.CLASSES), 3))

        self.NET = cv2.dnn.readNet(self.YOLO_WEIGHTS, self.YOLO_DNN)

        self.OUTPUT_LAYERS = self.get_output_layers(self.NET)

    # Get output layers
    def get_output_layers(self, net):
        layer_names = net.getLayerNames()
        output_layers = [layer_names[i[0] - 1] for i in net.getUnconnectedOutLayers()]
        return output_layers


    # Generate blob out of image
    def get_blob(self, img, scale=None):
        return cv2.dnn.blobFromImage(img, self.SCALE, (416,416), (0,0,0), True, crop=False)

    # Feed forward a blob
    def get_dnn_results(self, blob):
        self.NET.setInput(blob)
        return self.NET.forward(self.OUTPUT_LAYERS)

    # Calculate bounding box
    def create_box_from_detection(self, detection, im_width, im_height):
        center_x = int(detection[0] * im_width)
        center_y = int(detection[1] * im_height)
        w = int(detection[2] * im_width)
        h = int(detection[3] * im_height)
        x = center_x - w / 2
        y = center_y - h / 2
        return (x, y, w, h)

    # Merge boxes that are similar
    def merge_boxes(self, boxes, confidences):
        return cv2.dnn.NMSBoxes(boxes, confidences, self.CONF_THRES, self.NMS_THRES)

    # Get prediction boxes
    def get_prediction_boxes(self, outs, im_width, im_height):
        boxes = []
        class_ids = []
        confidences = []
        for out in outs:
            for detection in out:
                scores = detection[5:]
                class_id = np.argmax(scores)
                
                class_ids.append(class_id)
                confidences.append(float(scores[class_id]))
                boxes.append(self.create_box_from_detection(detection, im_width, im_height))
                
        return boxes, class_ids, confidences

    # Create a prediction for the image
    def predict(self, frame, count=-1, thread=-1):

        # Get an image blob
        blob = self.get_blob(frame)
            
        # Get the neural network outputs
        outs = self.get_dnn_results(blob)

        # Get predictions
        h, w, c = frame.shape
        boxes, class_ids, confidences = self.get_prediction_boxes(outs, w, h)

        # Only keep the best results for the boxes
        merged = self.merge_boxes(boxes, confidences)

        # Get the merged lists
        _boxes = [boxes[i[0]] for i in merged]
        _classes = [class_ids[i[0]] for i in merged]
        _confidences = [confidences[i[0]] for i in merged]

        classes = [ self.CLASSES[_id] for _id in _classes ]
        print('Thread {}, {} / Predicted labels: '.format(thread, count) + str(classes))
        
        return _boxes, _classes, _confidences